﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{

    public int health;
    public int mana;
    public int str;
    public int agi;
    public int intell;
    
}
