**-Movimiento Pj:**

- El pj se moverá al darle clic en el botón derecho del ratón, no podrás avanzar mas de 5 casillas.

**-Menú:**


- Tenemos las opciones de jugar y cargar, esta ultima, si antes guardaste la partida te cargara con todos tus datos del pj.
- Si no le das a cargar y le das a jugar, comenzaras desde 0

**-Objetivos :**


- Spam de enemigos aleatoriamente por el mapa

- Barras de vida en los enemigos

- Despliegue de un menú con opciones de atacar, usar magia y un inventario

- Exp del personaje al matar a los monstruos

- Skill restringidas por nivel

- Cambio de escena

- GameOver al llegar a 0 nuestra vida

- Persistencia entre las escenas

- Sistema de partículas 

- Sistema de turnos entre enemigos y aliado

- ScriptableObjects
