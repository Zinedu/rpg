﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataObject
{
    public int hp;
    public int maxHp;
    public int mana;
    public int maxMana;
    public int gold;
    public int xp;
    public int xpToNextLevel;
    public int stamina;
    public int iniciativa;
    public int lvl;

    public DataObject(int hp, int maxHp, int mana, int maxMana, int gold, int xp, int xpToNextLevel, int stamina, int iniciativa, int lvl)
    {
        this.hp = hp;
        this.maxHp = maxHp;
        this.mana = mana;
        this.maxMana = maxMana;
        this.gold = gold;
        this.xp = xp;
        this.xpToNextLevel = xpToNextLevel;
        this.stamina = stamina;
        this.iniciativa = iniciativa;
        this.lvl = lvl;
    }


}
