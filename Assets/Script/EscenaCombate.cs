﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EscenaCombate : MonoBehaviour
{
    //Selecion de escena para cambiar
    [SerializeField]
    private string Scenes;

    //colision y cambio de escena
    void OnTriggerEnter2D(Collider2D other)
    {
        if (SceneManager.GetActiveScene().name == "Overworld_1") {
        if (other.CompareTag("Player"))
        {
            SceneManager.LoadScene(Scenes);
        }
    }
    }
}
