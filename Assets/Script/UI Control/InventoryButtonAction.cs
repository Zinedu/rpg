﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButtonAction : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(this.gameObject.GetComponent<Transform>().GetChild(0).gameObject.GetComponent<Text>().text == "Pocion")
        {
            this.gameObject.GetComponent<Button>().onClick.AddListener(PocionUse);
        }
        if (this.gameObject.GetComponent<Transform>().GetChild(0).gameObject.GetComponent<Text>().text == "Granada")
        {
            this.gameObject.GetComponent<Button>().onClick.AddListener(GranadaUse);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void PocionUse()
    {
        Stats.hp = Stats.hp += 50;
        if(Stats.hp > Stats.maxHp)
        {
            Stats.hp = Stats.maxHp;
        }
        ScriptableObject pcn = ScriptableObject.CreateInstance("Item");
        pcn.name = "Pocion";
        PlayerBag.RemoveFromBag(pcn);
    }

    void GranadaUse()
    {
        GameObject[] enemyList = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject en in enemyList)
        {
            en.GetComponent<EnemyMono>().LifeDown(10);
        }
        ScriptableObject grnd = ScriptableObject.CreateInstance("Item");
        grnd.name = "Granada";
        PlayerBag.RemoveFromBag(grnd);
    }
}
