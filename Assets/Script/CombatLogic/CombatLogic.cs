﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CombatLogic : MonoBehaviour
{
    List<GameObject> combatOrder = new List<GameObject>();
    public int turnPointer = 0;
    public bool playerTurn = true;
    public static bool usableUI = true;

    // Start is called before the first frame update
    void Start()
    {
        GameObject[] combatants = GameObject.FindGameObjectsWithTag("Enemy");

        foreach(GameObject g in combatants)
        {
            combatOrder.Add(g);
        }
        AttackAlly.OnAttack += PlayerAttackSelected;
        EnemyCombat.OnEnemyAttack += EnemyAttackEvent;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerTurn)
        {
            usableUI = true;
        }
        else
        {
            usableUI = false;
        }
        combatDone();
        if(Stats.hp <= 0)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }

    void AdvanceTurn()
    {
        combatOrder[turnPointer].GetComponent<EnemyCombat>().thisTurn = false;
        turnPointer++;
        if(turnPointer >= combatOrder.Count)
        {
            turnPointer = 0;
            playerTurn = true;
            SanityCheck();
        }

        if (!playerTurn)
        {
            SanityCheck();
            combatOrder[turnPointer].GetComponent<EnemyCombat>().thisTurn = true;
        }
    }

    void PlayerAttackSelected()
    {
        SanityCheck();
        playerTurn = false;
        Debug.Log("Evento de Cambio de turno aliado");
        if (GameObject.Find("AttackPanel"))
        {
            GameObject.Find("AttackPanel").SetActive(false);
        }
        combatOrder[turnPointer].GetComponent<EnemyCombat>().thisTurn = true;
        if (GameObject.Find("MagicPanel"))
        {
            GameObject.Find("MagicPanel").SetActive(false);
        }
        if (DebuffStatus.currentStatus.Contains("veneno"))
        {
            Stats.hp -= 5;
        }
    }

    void EnemyAttackEvent()
    {
        AdvanceTurn();
    }

    void SanityCheck()
    {
        combatOrder.Clear();
        GameObject[] combatants = GameObject.FindGameObjectsWithTag("Enemy");

        foreach (GameObject g in combatants)
        {
            combatOrder.Add(g);
        }
    }

    void combatDone()
    {
        if (GameObject.FindGameObjectsWithTag("Enemy").Length == 0) {
            SceneManager.LoadScene("OverWorld_1");
        }
    }

}
