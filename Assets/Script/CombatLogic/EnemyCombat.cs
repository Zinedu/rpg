﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCombat : MonoBehaviour
{
    public bool thisTurn = false;
    public delegate void enemyAttack();
    public static event enemyAttack OnEnemyAttack;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (thisTurn)
        {
            attack();
            thisTurn = false;
        }
    }

    void attack() { 
    
        if(OnEnemyAttack != null)
        {
            int rng = Random.Range(-6, 10);
            if (rng > 0){
                Stats.hp -= rng;
                Debug.Log(rng);
            }
            else
            {
                Debug.Log("Enemy missed");
            }
            Debug.Log("Evento de attack enemigo");
            GameObject.Find("playerParticle").GetComponent<ParticleSystem>().Play();
            OnEnemyAttack();
        }
    }

}
