﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatSpawner : MonoBehaviour
{
    Dictionary<string, Vector2> possibleSpawns = new Dictionary<string, Vector2>();
    List<string> selectedSpawns = new List<string>();
    
    // Start is called before the first frame update
    void Start()
    {
        possibleSpawns.Add("a", new Vector2(-17.50f, -1.44f));
        possibleSpawns.Add("b", new Vector2(-14.5f, 0.6f));
        possibleSpawns.Add("c", new Vector2(-11f, 0.8f));
        possibleSpawns.Add("d", new Vector2(-3.50f, 2));
        possibleSpawns.Add("e", new Vector2(1, 0.35f));
        possibleSpawns.Add("f", new Vector2(4.50f, -2.90f));

        int numberOfEnemies = Random.Range(2, 4);
        List<int> nonRepeatCheck = new List<int>();
        for(int i = 0; i < numberOfEnemies; i++)
        {
            int temp = Random.Range(0, 5);
            if (nonRepeatCheck.Contains(temp))
            {
                while (nonRepeatCheck.Contains(temp))
                {
                    temp = Random.Range(0, 5);
                }
            }
            nonRepeatCheck.Add(temp);
        }
        foreach(int a in nonRepeatCheck)
        {
            switch (a)
            {
                case 0:
                    selectedSpawns.Add("a");
                    break;
                case 1:
                    selectedSpawns.Add("b");
                    break;
                case 2:
                    selectedSpawns.Add("c");
                    break;
                case 3:
                    selectedSpawns.Add("d");
                    break;
                case 4:
                    selectedSpawns.Add("e");
                    break;
                case 5:
                    selectedSpawns.Add("f");
                    break;
            }
        }

        foreach(string s in selectedSpawns)
        {
            int randomMob = Random.Range(0, 4);
            switch (randomMob)
            {
                case 0:
                    GameObject.Instantiate(Resources.Load("Enemigos Prefabs/BossAraña"), new Vector2(possibleSpawns[s].x, possibleSpawns[s].y), Quaternion.identity);
                    break;
                case 1:
                    GameObject.Instantiate(Resources.Load("Enemigos Prefabs/BossCalamar"), new Vector2(possibleSpawns[s].x, possibleSpawns[s].y), Quaternion.identity);
                    break;
                case 2:
                    GameObject.Instantiate(Resources.Load("Enemigos Prefabs/BossDino"), new Vector2(possibleSpawns[s].x, possibleSpawns[s].y), Quaternion.identity);
                    break;
                case 3:
                    GameObject.Instantiate(Resources.Load("Enemigos Prefabs/BossRata"), new Vector2(possibleSpawns[s].x, possibleSpawns[s].y), Quaternion.identity);
                    break;
            }
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
