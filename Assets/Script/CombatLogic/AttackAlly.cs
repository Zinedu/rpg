﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class AttackAlly : MonoBehaviour
{
    //Nota: algunos ataques requeriran experiencia para desbloquearlos.
    public int dmg;
    //GameObject actualplayer;
    public delegate void choice();
    public static event choice OnAttack;
    void Start()
    {
        //actualplayer = GameObject.FindWithTag("Player");
        this.GetComponent<Collider2D>().enabled = false;
        
        
    }


    public void normalAttack(Ataques at)
    {
        //Si supera del nivel indicado, entra, si no, te pedira que tengas x nivel para desbloquear el ataque.
        if(Stats.lvl>= at.lvl)
        {
            //El porcentaje de fallar seria mas o menos un 25%. Si el random es un numero negativo, significa que has fallado, si no, aciertas y haces un ataque entre 1 y el mañimo del scriptableobject.
            int failchance = -(at.daño / 2);
            int i = Random.Range(failchance, at.daño);
            if (i <= 0)
            {
                Debug.Log("You Missed");
            }
            else
            {
                Debug.Log("You Hit");
                this.dmg = i;
                Debug.Log("dmg = " + this.dmg);
                //Tengo que ponerlo aqui porque el normalAttack solo es llamado por un boton, y como no entra al start ni al update, tengo que inicializar la variable desde aqui.
                GameObject actualplayer = GameObject.FindWithTag("Player");
                //StartCoroutine(attack(actualplayer));
                actualplayer.GetComponent<AttackAlly>().StartCoroutine(attack());
            }
            
            if (OnAttack != null)
            {
                Debug.Log(OnAttack);
                OnAttack();
                Debug.Log("Evento de attack aliado");
            }
            
        }
        else
        {
            Debug.Log("Reach the level " + at.lvl + " to unlock this attack");
        }


    }
    public void manaAttack(Hechizos hc)
    {
        
        
        if (Stats.lvl >= hc.lvl)
        {
            //Hago un calculo de mana guardando el mana actual por si las moscas, en caso de que un hechizo gaste mucho mana y llegue a negativo, y asi no hacer calculos editando directamente la clase Stats.
            int manc = Stats.mana;
            if (manc - hc.mana >= 0)
            {
                Stats.mana -= hc.mana;
                //De momento siempre le hara hit y no fallara en el mana.
                this.dmg = Random.Range(1, hc.daño);
                GameObject actualplayer = GameObject.FindWithTag("Player");
                actualplayer.GetComponent<AttackAlly>().StartCoroutine(attack());
                if (OnAttack != null)
                {
                    OnAttack();
                    Debug.Log("Evento de attack aliado");
                }
            }
            else
            {
                Debug.Log("You don't have enough mana");
            }
        }
        else
        {
            Debug.Log("Reach the level " + hc.lvl + " to unlock this magic attack");
        }
        

    }




    IEnumerator attack()
    {
        //Debug.Log("entrando a attack");
        GameObject actualplayer = GameObject.FindWithTag("Player");
        //Funcion que implementaremos para los ataques
        actualplayer.GetComponent<Collider2D>().enabled = true;
        //Cuando acabe el frame, vuelve y desactiva el trigger. En resumen, al frame 1 se activa, mirara que gameobjects ha entrado en el trigger y al 2o frame, se desactiva
        yield return new WaitForSeconds(0.1f);
        actualplayer.GetComponent<Collider2D>().enabled = false;
    }



    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("COLISION");
        if (collision.tag== "Enemy")
        {
            Debug.Log(collision.name + " damage taken" + this.dmg);
            collision.GetComponent<EnemyMono>().LifeDown(this.dmg);
        }
        
    }

}
